/*
 * File:   main.c
 * Author: alx
 *
 * Created on 7 de diciembre de 2013, 10:28 AM
 */
/*
GND     P9_1
VCC     P9_3
TX      P9_24
RX      P9_26
*/

#include <stdio.h>
#include <stdlib.h>
#include "BBB.h"

char buffer[255];   /* Input buffer */
int size_buff = 0;

int  readLineUART(const uint8_t UARTMOD, void *Buffer,  int length)
{
    char *_bufptr = NULL;    /* Current char in buffer */
    int _nbytes = 0;         /* Number of bytes read */
    int _size_buff = 0;
    _bufptr = Buffer;
    while ((_nbytes = readUART(UARTMOD, _bufptr, 1, 10)) > 0)
    {
        _bufptr += _nbytes;
        *_bufptr = '\0';
        _size_buff += _nbytes;
        if (_size_buff >= length)
        {
            break;
        }
        if (_bufptr[-1] == '\n')
        {
            break;
        }
        if (_bufptr[-1] == 0)
        {
            break;
        }
    }
    return _size_buff;
}
/*
 *
 */
int main()
{
    int tty_fd;
    unsigned char c = 'D';
    Load_libfx();
    LoadUART(UART1);
    tty_fd = setSpeedUART(UART1, 115200);
    printf("hola \r\n");
    // while (1) {

    //     size_buff=readLineUART(UART1,buffer,sizeof(buffer));

    //     if(size_buff>0){
    //         printf("data rx:%s ,size %d\r\n",buffer,size_buff);
    //         size_buff = 0;
    //     }

    // }
    while (c != 'q')
    {
        if (read(tty_fd, &c, 1) > 0)
            printf("%c", c);
        //     write(STDOUT_FILENO, &c, 1);        // if new data is available on the serial port, print it out
        //if (read(STDIN_FILENO, &c, 1) > 0)  write(tty_fd, &c, 1);               // if new data is available on the console, send it to the serial port
    }

    close(tty_fd);
    return (EXIT_SUCCESS);
}

