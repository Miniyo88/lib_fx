/* 
 * File:   main.c
 * Author: alx
 *
 * Created on 7 de diciembre de 2013, 10:28 AM
 */
  /*
GND     P9_1
VCC     P9_3
TX      P9_24
RX      P9_26
 */

#include <stdio.h>
#include <stdlib.h>
#include "BBB.h"

    char buffer[255];   /* Input buffer */
    int size_buff = 0;

  int  readLineUART(const uint8_t UARTMOD, void *Buffer,  int length){
        char *_bufptr=NULL;      /* Current char in buffer */
        int _nbytes=0;           /* Number of bytes read */
        int _size_buff = 0;
        _bufptr = Buffer;
        while ((_nbytes = readUART(UARTMOD, _bufptr, 1, 10)) > 0) {
            _bufptr += _nbytes;
            *_bufptr = '\0';
            _size_buff += _nbytes;
            if(_size_buff>=length){
                break;
            }
            if (_bufptr[-1] == '\n') {
                break;
            }
            if (_bufptr[-1] == 0) {
                break;
            }
        }
        return _size_buff;
    }

/*serial command*/
uint8_t command[256] = "\0";
uint8_t commandindex = 0;
uint8_t command_ok = 0;

uint8_t capture_command(void) {
    //char *_bufptr=NULL;      /* Current char in buffer */
    int _nbytes=0; 
    if ((_nbytes = readUART(UART1, &command[commandindex], 1, 10)) > 0) {
        //command[commandindex] = Serial_read();
        if (command[commandindex] == '#') {
            commandindex = 0;
            command[0] = '#';
            command_ok = 0;
        }

        if (command[0] == '#') {
            if (command[commandindex] != ':') {
                commandindex++;
            } else {
                command_ok = 1;
            }
            command[commandindex] = '\0';
        } else {
            command[commandindex] = '\0';
            commandindex = 0;
        }
        printf("comand buff :%s ,i:%d\r\n",command,commandindex);
    }
    return command_ok;
}

uint8_t read_command(void) {
    if (command_ok == 1) {
        switch (command[1]) {
            case 'a':

                if (command[2] == '0') {
                    printf("comando:a0\r\n");
                    break;
                }

                if (command[2] == '1') {
                    printf("comando:a1\r\n");
                    break;
                }
                break;
            case 'b':
                //STM_EVAL_LEDToggle(LED5); //led rojo
                break;
            case 'c':
                if (command[2] == 'x') {
                    printf("comando:cx\r\n");
                    break;
                }
                break;
            case 'o':
                /*calibrar angulo*/
                
                /*stop calc imu offset  */
                if (command[2] == '0') {
                    printf("comando:o0\r\n");
                    break;
                }
                /*start_flag calc imu offset */
                if (command[2] == '1') {
                    printf("comando:o1\r\n");
                    break;
                }
                break;
            case 'p':
                /*manual*/
                //float pvalue=0.f;
                if (command[2] == 'a') {
                    printf("comando:pa\r\n");
                    break;
                }
                if (command[2] == 'b') {
                    printf("comando:pb\r\n");
                    break;
                }
                if (command[2] == '0') {
                    printf("comando:pc\r\n");
                    break;
                }
                break;
            case 's':
                if (command[2] == '0') {
                    printf("comando:s0\r\n");
                    break;
                }

                if (command[2] == '1') {
                    printf("comando:s1\r\n");
                    break;
                }

                if (command[2] == 'C') {
                    printf("comando:sC\r\n");
                    break;
                }
                break;
            case 'x':
                if (command[2] == 'a') {
                    printf("comando:xa\r\n");
                    break;  
                }
                if (command[2] == 'b') {
                    printf("comando:xb\r\n");
                    break;
                }
                break;
            default:
                break;
        }
        command_ok = 0;
        commandindex = 0;
        command[0] = '\0';
    }
    return 1;
}

/*
 * 
 */
int main() {
    Load_libfx();
    LoadUART(UART1);
    setSpeedUART(UART1, 115200);
    printf("hola \r\n");
    while (1) {

    capture_command();
    read_command();

    }
    return (EXIT_SUCCESS);
}

