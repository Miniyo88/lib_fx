/*
 * File:   main.c
 * Author: alx
 *
 * Created on 7 de diciembre de 2013, 10:28 AM
 */
/*
GND     P9_1
VCC     P9_3
TX      P9_24
RX      P9_26
*/

//#include <stdio.h>
//#include <stdlib.h>
//#include <assert.h>
#include "BBB.h"
//#include "stm_ssp.h"
#include "gps.h"

//===========================================================================

char GpsBuffer[255];   /* Input GpsBuffer */
int size_GpsBuffer = 0;

int  readLineUART(const uint8_t UARTMOD, void *Buffer,  int length)
{
    char *_bufptr = NULL;    /* Current char in GpsBuffer */
    int _nbytes = 0;         /* Number of bytes read */
    int _size_buff = 0;
    _bufptr = Buffer;
    while ((_nbytes = readUART(UARTMOD, _bufptr, 1, 10)) > 0)
    {
        _bufptr += _nbytes;
        *_bufptr = '\0';
        _size_buff += _nbytes;
        if (_size_buff >= length)
        {
            break;
        }
        if (_bufptr[-1] == '\n')
        {
            break;
        }
        if (_bufptr[-1] == 0)
        {
            break;
        }
    }
    return _size_buff;
}
/*
 *
 */
int main()
{
    int nmea_type = 0;
    nmeaGPGGA dataGPGGA;
    //nmeaGPGSA dataGPGSA;
    //nmeaGPGSV dataGPGSV;
    nmeaGPRMC dataGPRMC;
    //nmeaGPVTG dataGPVTG;

    Load_libfx();
    LoadUART(UART1);
    setSpeedUART(UART1, 9600);
    printf("hola \r\n");



    while (1)
    {

        size_GpsBuffer = readLineUART(UART1, GpsBuffer, sizeof(GpsBuffer));

        if (size_GpsBuffer > 0)
        {
            nmea_type = nmea_pack_type(GpsBuffer, size_GpsBuffer);
            switch (nmea_type)
            {
            case GPGGA:  //ok
                if (nmea_parse_GPGGA(GpsBuffer, size_GpsBuffer, &dataGPGGA) == 0)
                {
                    printf("error can not parse GPGGA \r\n");
                    break;
                }
                break;
            case GPRMC:  //ok
                if (nmea_parse_GPRMC(GpsBuffer, size_GpsBuffer, &dataGPRMC) == 0)
                {
                    printf("error can not parse GPRMC \r\n");
                    break;
                }
                break;
            default:
                break;
            }
            size_GpsBuffer = 0;

            printf("Time:20%d-%d-%d %d:%d:%d.%d\r\n", dataGPRMC.utc.year, dataGPRMC.utc.mon, dataGPRMC.utc.day, dataGPRMC.utc.hour, dataGPRMC.utc.min, dataGPRMC.utc.sec, dataGPRMC.utc.hsec);
            printf("status:%c\r\n", dataGPRMC.status);
            printf("Latitude:%g %c\r\n", dataGPRMC.lat, dataGPRMC.ns);
            printf("Longitude:%g %c\r\n", dataGPRMC.lon, dataGPRMC.ew);
            printf("Speed:%g\r\n", dataGPRMC.speed);
            printf("Direction:%g\r\n", dataGPRMC.direction);
            printf("Declination:%g %c\r\n", dataGPRMC.declination, dataGPRMC.declin_ew);
            printf("Mode:%d\r\n", dataGPRMC.mode);
            printf("Quality:%d\r\n", dataGPGGA.sig);
            printf("Altitude:%g\r\n", dataGPGGA.elv);
        }

    }
    return (EXIT_SUCCESS);
}

