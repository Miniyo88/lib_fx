
//#include <stdio.h>
//#include <errno.h>

#include <stdlib.h>     /* strtol */
#include <math.h>
#include <string.h>
#include <float.h>
#include "stm_ssp.h"


typedef struct
{
    int BHead;
    int BTail;
    int BSize;
    char *Buffer;
} vsnp_t;

vsnp_t bfsnp;

int check_isdigit (char c);
int check_isupper(char c);
int check_isspace(int c);
int sscanf_atoi(const char *str, int str_sz, int radix);
float sscanf_atof(const char *str, int str_sz);

void _print_c(int c);
int _print_s(const char *string, int width, int pad);
int _print_i(int i, int b, int sg, int width, int pad);
int _print_f(float f, int width, int precision, int pad);
int _VPrintf(const char *format, va_list args );
int _VSnprintf(char *out, int size, const char *format, va_list args);


/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param c [description]
 */
void _print_c(int c)
{
    extern int putchar(int c);

    if ((bfsnp.BSize > bfsnp.BHead))
    {
        bfsnp.Buffer[bfsnp.BHead] = c;
        bfsnp.BHead++;
    }
    else (void)putchar(c);
}

////////////////////////////////////////////////////////////////////////////

/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param ch [description]
 * @return [description]
 */
int a2d(char ch)
{
    if (ch >= '0' && ch <= '9')
        return ch - '0';
    else if (ch >= 'a' && ch <= 'f')
        return ch - 'a' + 10;
    else if (ch >= 'A' && ch <= 'F')
        return ch - 'A' + 10;
    else
        return -1;
}

/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param i [description]
 * @param a [description]
 * @param r [description]
 * @return [description]
 */
char *i2a(unsigned i, char *a, unsigned r)
{
    if (i / r > 0)
        a = i2a(i / r, a, r);
    *a = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"[i % r];
    return a + 1;
}

/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param i [description]
 * @param a [description]
 * @param digits [description]
 * @return [description]
 */
char *if2a(unsigned i, char *a, unsigned digits)
{
    if (digits > 0)
        a = if2a(i / 10, a, digits - 1);
    *a = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"[i % 10];
    return a + 1;
}

/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param i [description]
 * @param a [description]
 * @param r [description]
 * @return [description]
 */
char *itoa(int i, char *a, int r)
{
    if ((r < 2) || (r > 36))
        r = 10;
    if (i < 0)
    {
        *a = '-';
        *i2a(-i, a + 1, r) = 0;
    }
    else
        *i2a(i, a, r) = 0;

    return a;
}


/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param f [description]
 * @param int [description]
 * @param buf [description]
 * @return [description]
 */
float DIGITSD[7] = {1.0, 10.0, 100.0, 1000.0, 10000.0, 100000.0, 1000000.0};
int ftoax( float f, unsigned int digits, char *buf)
{
    long multiplier, int_part, part;
    char *pBUFFF = buf;
    if (f < 0.0f)
    {
        //_print_c ('-');
        *(pBUFFF++) = '-';
        f = -f;
    }
    if (isinf(f))
    {
        //strcpy(pBUFFF, "INF");
        *(pBUFFF++) = 'I';
        *(pBUFFF++) = 'N';
        *(pBUFFF++) = 'F';
        *(pBUFFF) = '\0';
        return 3;
    }
    else if (isnan(f))
    {
        //strcpy(pBUFFF, "NaN");
        *(pBUFFF++) = 'N';
        *(pBUFFF++) = 'A';
        *(pBUFFF++) = 'N';
        *(pBUFFF) = '\0';
        return 3;
    }
    else
    {

        if (digits > 6 )
            digits = 6;
        else if (digits <= 0)
            digits = 3;
        multiplier = DIGITSD[digits];

        int_part = (long) f;
        part = (long) ((f - floorf(f)) * multiplier + 0.5f);
        pBUFFF = i2a(int_part, pBUFFF, 10);
        if (digits > 0)
        {
            *(pBUFFF++) = '.';
            pBUFFF = if2a(part, pBUFFF, digits - 1);
        }
        *(pBUFFF) = '\0';
        return pBUFFF - buf;

    }
    return 0;
}
//////////////////////////////////////////////////////////////////////////////

#define SSCANF_CONVSTR_BUF    (256)
#define SSCANF_TOKS_COMPARE   (1)
#define SSCANF_TOKS_PERCENT   (2)
#define SSCANF_TOKS_WIDTH     (3)
#define SSCANF_TOKS_TYPE      (4)

/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param c [description]
 * @return [description]
 */
int check_isdigit (char c)
{
    return (c >= '0') && (c <= '9');
}
/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param c [description]
 * @return [description]
 */
int check_isupper(char c)
{
    return (c >= 'A' && c <= 'Z');
}

/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param c [description]
 * @return [description]
 */
int check_isspace(int c)
{
    return ((c >= 0x09 && c <= 0x0D) || (c == 0x20));
}
/**
 * \brief Convert string to number
 */
/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param str [description]
 * @param str_sz [description]
 * @param radix [description]
 * @return [description]
 */
int sscanf_atoi(const char *str, int str_sz, int radix)
{
    char *tmp_ptr;
    char buff[SSCANF_CONVSTR_BUF];
    int res = 0;

    if (str_sz < SSCANF_CONVSTR_BUF)
    {
        memcpy(&buff[0], str, str_sz);
        buff[str_sz] = '\0';
        res = strtol(&buff[0], &tmp_ptr, radix);
    }

    return res;
}

/**
 * \brief Convert string to fraction number
 */
/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param str [description]
 * @param str_sz [description]
 *
 * @return [description]
 */
float sscanf_atof(const char *str, int str_sz)
{
    char *tmp_ptr;
    char buff[SSCANF_CONVSTR_BUF];
    float res = 0;

    if (str_sz < SSCANF_CONVSTR_BUF)
    {
        memcpy(&buff[0], str, str_sz);
        buff[str_sz] = '\0';
        res = strtod(&buff[0], &tmp_ptr);

    }

    return res;
}

/**
 * \brief Analyse string (specificate for SSCANF sentences)
 */
/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param buff [description]
 * @param buff_sz [description]
 * @param format [description]
 * @return [description]
 */
int sscanf_scanf(const char *buff, int buff_sz, const char *format, ...)
{
    const char *beg_tok;
    const char *end_buf = buff + buff_sz;

    va_list arg_ptr;
    int tok_type = SSCANF_TOKS_COMPARE;
    int width = 0;
    const char *beg_fmt = 0;
    int snum = 0, unum = 0;

    int tok_count = 0;
    void *parg_target;

    VA_START(arg_ptr, format);

    for (; *format && buff < end_buf; ++format)
    {
        switch (tok_type)
        {
        case SSCANF_TOKS_COMPARE:
            //
            if ('%' == *format)
                tok_type = SSCANF_TOKS_PERCENT;
            else if (*buff++ != *format)
                //----------------------------------------------
                goto fail;
            //
            break;
        case SSCANF_TOKS_PERCENT:
            //
            width = 0;
            beg_fmt = format;
            tok_type = SSCANF_TOKS_WIDTH;
        //
        case SSCANF_TOKS_WIDTH:
            //
            if (check_isdigit(*format))
                break;
            {
                tok_type = SSCANF_TOKS_TYPE;
                if (format > beg_fmt)
                {
                    width = sscanf_atoi(beg_fmt, (int) (format - beg_fmt), 10);
                }
            }
        //
        case SSCANF_TOKS_TYPE:
            //
            beg_tok = buff;
            if (!width && ('c' == *format || 'C' == *format) && *buff != format[1])
                width = 1;

            if (width)
            {
                if (buff + width <= end_buf)
                    buff += width;
                else
                    goto fail;
            }
            else
            {
                if (!format[1] || (0 == (buff = (char *) memchr(buff, format[1], end_buf - buff))))
                    buff = end_buf;
            }

            if (buff > end_buf)
                //----------------------------------------------
                goto fail;

            tok_type = SSCANF_TOKS_COMPARE;
            tok_count++;

            parg_target = 0;
            width = (int) (buff - beg_tok);



            //P2--------------------------------------------------
            switch (*format)
            {
            case 'c':
            case 'C':
                parg_target = (void *) VA_ARG(arg_ptr, char *);
                if (width && 0 != (parg_target))
                {
                    *((char *) parg_target) = *beg_tok;
                }
                break;
            case 's':
            case 'S':
                parg_target = (void *) VA_ARG(arg_ptr, char *);
                if (width && 0 != (parg_target))
                {
                    memcpy(parg_target, beg_tok, width);
                    ((char *) parg_target)[width] = '\0';
                }
                break;
            case 'f':
            case 'g':
            case 'G':
            case 'e':
            case 'E':
                parg_target = (void *) VA_ARG(arg_ptr, float *);
                if (width && 0 != (parg_target))
                {
                    *((float *) parg_target) = sscanf_atof(beg_tok, width);
                }
                break;
            };

            //P3--------------------------------------------------
            if (parg_target)
                break;
            if (0 == (parg_target = (void *) VA_ARG(arg_ptr, int *)))
                break;
            if (!width)
                break;

            switch (*format)
            {
            case 'd':
            case 'i':
                snum = sscanf_atoi(beg_tok, width, 10);
                memcpy(parg_target, &snum, sizeof (int));
                break;
            case 'u':
                unum = sscanf_atoi(beg_tok, width, 10);
                memcpy(parg_target, &unum, sizeof (unsigned int));
                break;
            case 'x':
            case 'X':
                unum = sscanf_atoi(beg_tok, width, 16);
                memcpy(parg_target, &unum, sizeof (unsigned int));
                break;
            case 'o':
                unum = sscanf_atoi(beg_tok, width, 8);
                memcpy(parg_target, &unum, sizeof (unsigned int));
                break;
            default:
                //----------------------------------------------

                goto fail;
            };

            break;
        };
    }

fail:

    VA_END(arg_ptr);

    return tok_count;
}

///////////////////////////////////////////////////////////////////////////////

#define PRINT_BUF_LEN 32
#define PAD_RIGHT   1
#define PAD_ZERO    2
#define PAD_FDIG    4
#define PAD_SIG     5

/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param out [description]
 * @param string [description]
 * @param width [description]
 * @param pad [description]
 * @return [description]
 */
int _print_s(const char *string, int width, int pad)
{
    register int pc = 0, padchar = ' ';

    if (width > 0)
    {
        register int len = 0;
        register const char *ptr;
        for (ptr = string; *ptr; ++ptr)
        {
            ++len;
        }
        if (len >= width)
        {
            width = 0;
        }
        else
        {
            width -= len;
        }
        if (pad & PAD_ZERO)
        {
            padchar = '0';
        }
    }

    if (!(pad & PAD_RIGHT))
    {
        for ( ; width > 0; --width)
        {
            _print_c ( padchar);
            ++pc;
        }
    }
    for ( ; *string ; ++string)
    {
        _print_c ( *string);
        ++pc;
    }
    for ( ; width > 0; --width)
    {
        _print_c ( padchar);
        ++pc;
    }

    return pc;
}


/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param i [description]
 * @param b [description]
 * @param sg [description]
 * @param width [description]
 * @param pad [description]
 * @return [description]
 */
int _print_i(int i, int b, int sg, int width, int pad)
{
    char print_buf[PRINT_BUF_LEN];
    char *p_buff = 0;
    int size = width;
    unsigned int u = i;
    if (sg && b == 10 && i < 0)
    {
        print_buf[0] = '-';
        p_buff = i2a(-i, print_buf + 1, b);
        //p_buff = itoa(i, print_buf, b);
    }
    else
    {
        p_buff = i2a(u, print_buf, b);
    }
    if (size > 0 && (pad & PAD_RIGHT))
    {
        size -= (p_buff - print_buf);
        while (size > 0)
        {
            *(p_buff++) = ' ';
            size--;
        }
    }
    *p_buff = 0;
    return _print_s ( print_buf, width, pad);
}




/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param f [description]
 * @param width [description]
 * @param precision [description]
 * @param pad [description]
 * @return [description]
 */
int _print_f(float f, int width, int precision, int pad)
{
    char print_buf[PRINT_BUF_LEN];
    int size = width;
    int sizef = 0;
    char *p_buff = print_buf;
    sizef = ftoax(f, (unsigned int)precision, print_buf);
    p_buff += sizef;
    if (size > 0)
    {
        if (sizef >= size)
        {
            print_buf[size] = 0;
            sizef = size;
        }
        if ((pad & PAD_RIGHT))
        {
            size -= (sizef);
            while (size > 0)
            {
                *(p_buff++) = ' ';
                size--;
            }
        }
    }

    *p_buff = 0;
    return _print_s ( print_buf, width, pad);
}

/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param out [description]
 * @param format [description]
 * @param args [description]
 * @return [description]
 */
int _VPrintf(const char *format, va_list args )
{
    register int width, precision, pad;
    register int pc = 0;
    char scr[2];
    char *s = 0;
    float pREAL;
    for (; *format != 0; ++format)
    {
        if (*format == '%')
        {
            ++format;
            width = precision = pad = 0;
            if (*format == '\0')
            {
                break;
            }
            if (*format == '%')
            {
                goto out;
            }
            if (*format == '-')
            {
                ++format;
                pad = PAD_RIGHT;
            }

            while (*format == '0')
            {
                ++format;
                pad |= PAD_ZERO;
            }

            for ( ; *format >= '0' && *format <= '9'; ++format)
            {
                width *= 10;
                width += *format - '0';
            }

            if (*format == '.')
            {
                ++format;
                pad |= PAD_FDIG;

                for ( ; *format >= '0' && *format <= '9'; ++format)
                {
                    precision *= 10;
                    precision += *format - '0';
                }
            }
            switch (*format)
            {
            case 's' :
            case 'S' :
                s = (char *)VA_ARG( args, char *);  // a revoir
                pc += _print_s ( s ? s : "(null)", width, pad);
                break;
            case 'd' :
            case 'i' :
                pc += _print_i ( VA_ARG( args, int ), 10, 1, width, pad);
                break;
            case 'u' :
                pc += _print_i ( VA_ARG( args, int ), 10, 0, width, pad);
                break;
            case 'r' :
            case 'R' :
                pREAL = *((float *)VA_ARG( args, float *));
                pc += _print_f(pREAL, width, precision,  pad);
                break;
            case 'f' :
            case 'g' :
            case 'G' :
            case 'e' :
            case 'E' :
                pc += _print_f((float)VA_ARG( args, double ), width, precision,  pad);
                break;
            case 'x' :
            case 'X' :
                pc += _print_i ( VA_ARG( args, int ), 16, 0, width, pad);
                break;
            case 'o' :
            case 'O' :
                pc += _print_i ( VA_ARG( args, int ), 8, 0, width, pad);
                break;
            case 'c' :
            case 'C' :
                scr[0] = (char)VA_ARG( args, int );
                scr[1] = '\0';
                pc += _print_s ( scr, width, pad);
                break;
            default:
                break;
            }
        }
        else
        {
out:
            _print_c (*format);
            ++pc;
        }
    }
    VA_END( args );
    return pc;
}

/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param out [description]
 * @param size [description]
 * @param format [description]
 * @param args [description]
 * @return [description]
 */
int _VSnprintf(char *out, int size, const char *format, va_list args)
{
    //va_list args;
    int status = 0;
    bfsnp.BSize = size;
    bfsnp.BHead = 0;
    bfsnp.Buffer = out;

    if ((bfsnp.BSize > 0) && (bfsnp.Buffer != 0))
    {
        //VA_START( args, format );
        status = _VPrintf(format, args );

        if (bfsnp.BHead < (bfsnp.BSize - 1))
        {
            bfsnp.Buffer[bfsnp.BHead] = '\0';
        }
        else
        {
            bfsnp.Buffer[bfsnp.BSize - 1] = '\0';
        }
    }

    bfsnp.BSize = 0;
    bfsnp.BHead = 0;
    bfsnp.Buffer = 0;

    return status;
}

/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param format [description]
 * @return [description]
 */
int Printf(const char *format, ...)
{
    va_list args;
    bfsnp.BSize = 0;
    bfsnp.BHead = 0;
    bfsnp.Buffer = 0;
    VA_START( args, format );
    return _VPrintf(format, args );
}

/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param out [description]
 * @param size [description]
 * @param format [description]
 * @return [description]
 */
int Snprintf(char *out, int size, const char *format, ...)
{
    va_list args;
    int status = 0;
    bfsnp.BSize = size;
    bfsnp.BHead = 0;
    bfsnp.Buffer = out;
    VA_START( args, format );
    status = _VSnprintf(out, size, format, args);
    return status;
}

