/* 
 * File:   main.c
 * Author: alx
 *
 * Created on 7 de diciembre de 2013, 10:28 AM
 */

#include <stdio.h>
#include <stdlib.h>
#include "BBB.h"
/**THIS IS A EXAMPLE OF QEI MODULE
 * 
 * QEI0 ONLY BEAGLEBOEN BLACK
 *P9_41 = GPIO3_20 = EQEP0_index       
 *P9_25 = GPIO3_21 = EQEP0_strobe  
 *P9_42 = GPIO3_18 = EQEP0A_in       
 *P9_27 = GPIO3_19 = EQEP0B_in    
 * 
 * QEI1
 *P8_31 = GPIO0_10 = EQEP1_index
 *P8_32 = GPIO0_11 = EQEP1_strobe
 *P8_33 = GPIO0_9 = EQEP1B_in
 *P8_35 = GPIO0_8 = EQEP1A_in
 * 
 *  QEI2
 * P8_39 = GPIO2_12 = EQEP2_index
 * P8_40 = GPIO2_13 = EQEP2_strobe
 * P8_41 = GPIO2_10 = EQEP2A_in
 * P8_42 = GPIO2_11 = EQEP2B_in
 */

int32_t qei0_poss = 0;

/*
 * 
 */
int main() {
    Load_libfx();
    LoadQEI(QEI0);
    setFrecuencyQEI(QEI0, 1000);
    setModeQEI(QEI0, 0);
    setEnableQEI(QEI0);
    setPositionQEI(QEI0, 0); /*warnig bug in kernel module*/
    while (1) {
        usleep(100000);
        qei0_poss = getPositionQEI(QEI0);
        printf("qei0:%ld\r\n", qei0_poss);
    }
    return (EXIT_SUCCESS);
}

