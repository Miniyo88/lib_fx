/**
 ******************************************************************************
 * @file    
 * @author  FREDY ALEXANDER SUÁREZ CÁRDENAS
 * @version V
 * @date    05-March-2012
 * @brief   
 *
 ******************************************************************************
 * @attention
 *
 *  COPYRIGHT 2013 FREDY ALEXANDER SUÁREZ CÁRDENAS
 *
 *
 ******************************************************************************
 */
#include "BBB.h"

/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param baseAdd [description]
 * @param pinNumber [description]
 *
 * @return [description]
 */
BitStatus ReadGPIOPin(uint32_t baseAdd, uint16_t pinNumber) {
    return (HWREG(baseAdd + GPIO_DATAIN) & (1 << pinNumber));
}

/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param baseAdd [description]
 * @param pinNumber [description]
 * @param pinValue [description]
 */
void WriteGPIOPin(uint32_t baseAdd, uint16_t pinNumber, uint16_t pinValue) {
    if (GPIO_PIN_HIGH == pinValue) {
        HWREG(baseAdd + GPIO_SETDATAOUT) = (1 << pinNumber);
    } else {
        HWREG(baseAdd + GPIO_CLEARDATAOUT) = (1 << pinNumber);
    }
}

/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param baseAdd [description]
 * @param pinNumber [description]
 * @param pinDirection [description]
 */
void SetGPIODirMode(uint32_t baseAdd, uint16_t pinNumber, uint16_t pinDirection) {
    if (GPIO_DIR_OUTPUT == pinDirection) {
        HWREG(baseAdd + GPIO_OE) &= ~(1 << pinNumber);
    } else {
        HWREG(baseAdd + GPIO_OE) |= (1 << pinNumber);
    }
}

/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param baseAdd [description]
 * @param pinNumber [description]
 *
 * @return [description]
 */
BitStatus GetGPIODirMode(uint32_t baseAdd, uint16_t pinNumber) {
    uint16_t retVal = GPIO_DIR_INPUT;

    if (!(HWREG(baseAdd + GPIO_OE) & (1 << pinNumber))) {
        retVal = GPIO_DIR_OUTPUT;
    }
    return retVal;
}

/***********************************************************************/



