/**
 ******************************************************************************
 * @file    
 * @author  FREDY ALEXANDER SUÁREZ CÁRDENAS
 * @version V
 * @date    05-March-2012
 * @brief   
 *
 ******************************************************************************
 * @attention
 *
 *  COPYRIGHT 2013 FREDY ALEXANDER SUÁREZ CÁRDENAS
 *
 *
 ******************************************************************************
 */
#include "BBB.h"
#include "BBB_HEADER.h"

// gpio exports

struct gpio_exp {
    unsigned int gpio;
    struct gpio_exp *next;
};
int nr_exported_gpio = 0;
struct gpio_exp *exported_gpios = NULL;
/******************************************************************************/

/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param int [description]
 * @return [description]
 */
ErrorStatus gpio_export(unsigned int gpio) {
    int fd, len;
    char str_gpio[10];
    struct gpio_exp *new_gpio, *g;

    if ((fd = open("/sys/class/gpio/export", O_WRONLY)) < 0) {
        return ERROR;
    }
    len = snprintf(str_gpio, sizeof (str_gpio), "%d", gpio);
    write(fd, str_gpio, len);
    close(fd);

    // add to list
    new_gpio = malloc(sizeof (struct gpio_exp));
    if (new_gpio == 0)
        return ERROR; // out of memory

    new_gpio->gpio = gpio;
    new_gpio->next = NULL;

    if (exported_gpios == NULL) {
        // create new list
        exported_gpios = new_gpio;
    } else {
        // add to end of existing list
        g = exported_gpios;
        while (g->next != NULL)
            g = g->next;
        g->next = new_gpio;
    }
    nr_exported_gpio++;
    return SUCCESS;
}
/******************************************************************************/

/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param int [description]
 * @return [description]
 */
ErrorStatus gpio_unexport(unsigned int gpio) {
    int fd, len;
    char str_gpio[10];
    struct gpio_exp *g, *temp, *prev_g = NULL;

    if ((fd = open("/sys/class/gpio/unexport", O_WRONLY)) < 0)
        return ERROR;

    len = snprintf(str_gpio, sizeof (str_gpio), "%d", gpio);
    write(fd, str_gpio, len);
    close(fd);

    // remove from list
    g = exported_gpios;
    while (g != NULL) {
        if (g->gpio == gpio) {
            if (prev_g == NULL)
                exported_gpios = g->next;
            else
                prev_g->next = g->next;
            temp = g;
            g = g->next;
            free(temp);
        } else {
            prev_g = g;
            g = g->next;
        }
    }
    return SUCCESS;
}

/******************************************************************************/

/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param GPIOP [description]
 * @param DIR [description]
 *
 * @return [description]
 */
ErrorStatus LoadGpioPin(uint16_t GPIOP, uint16_t DIR) {
    gpio_export(BBB_HEADER[GPIOP].GPIO);
    return setGpioPinDir(GPIOP, DIR);
}
/******************************************************************************/

/**
 * @brief [brief description]
 * @details [long description]
 */
void closeGpio(void) {
    // unexport everything
    while (exported_gpios != NULL)
        gpio_unexport(exported_gpios->gpio);
}
/*******************************API********************************************/

/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param GPIOP [description]
 * @param DIR [description]
 *
 * @return [description]
 */
ErrorStatus setGpioPinDir(uint16_t GPIOP, uint16_t DIR) {
    if (BBB_HEADER[GPIOP].GPBIT > 0 && BBB_HEADER[GPIOP].GPIOADD) {
        SetGPIODirMode((uint32_t) (MMAP_libfx + (BBB_HEADER[GPIOP].GPIOADD - PMMap_START_ADDR)), BBB_HEADER[GPIOP].GPBIT, DIR);
        return SUCCESS;
    } 
        return ERROR;
    
}

/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param GPIOP [description]
 * @return [description]
 */
BitStatus getGpioPinDir(uint16_t GPIOP) {
    if (BBB_HEADER[GPIOP].GPBIT > 0 && BBB_HEADER[GPIOP].GPIOADD) {
        return GetGPIODirMode((uint32_t) (MMAP_libfx + (BBB_HEADER[GPIOP].GPIOADD - PMMap_START_ADDR)), BBB_HEADER[GPIOP].GPBIT);
    } 
        return LOW;
    
}

/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param GPIOP [description]
 * @return [description]
 */
ErrorStatus setGpioPin(uint16_t GPIOP) {
    if (BBB_HEADER[GPIOP].GPBIT > 0 && BBB_HEADER[GPIOP].GPIOADD) {
        HWREG((uint32_t) (MMAP_libfx + (BBB_HEADER[GPIOP].GPIOADD - PMMap_START_ADDR)) + GPIO_SETDATAOUT) = (1 << BBB_HEADER[GPIOP].GPBIT);
        return SUCCESS;
    } 
        return ERROR;
    
}

/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param GPIOP [description]
 * @return [description]
 */
ErrorStatus clearGpioPin(uint16_t GPIOP) {
    if (BBB_HEADER[GPIOP].GPBIT > 0 && BBB_HEADER[GPIOP].GPIOADD) {
        HWREG((uint32_t) (MMAP_libfx + (BBB_HEADER[GPIOP].GPIOADD - PMMap_START_ADDR)) + GPIO_SETDATAOUT) = (1 << BBB_HEADER[GPIOP].GPBIT);
        return SUCCESS;
    } 
        return ERROR;
    
}

/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param GPIOP [description]
 * @return [description]
 */
BitStatus readGpioPin(uint16_t GPIOP) {
    if (BBB_HEADER[GPIOP].GPBIT > 0 && BBB_HEADER[GPIOP].GPIOADD) {
        return (int16_t) ReadGPIOPin((uint32_t) (MMAP_libfx + (BBB_HEADER[GPIOP].GPIOADD - PMMap_START_ADDR)), BBB_HEADER[GPIOP].GPBIT);
    }
    return LOW;
}

/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param GPIOP [description]
 * @param VALUE [description]
 *
 * @return [description]
 */
ErrorStatus writeGpioPin(uint16_t GPIOP, uint16_t VALUE) {
    if (BBB_HEADER[GPIOP].GPBIT > 0 && BBB_HEADER[GPIOP].GPIOADD) {
        WriteGPIOPin((uint32_t) (MMAP_libfx + (BBB_HEADER[GPIOP].GPIOADD - PMMap_START_ADDR)), BBB_HEADER[GPIOP].GPBIT, VALUE);
        return SUCCESS;
    } 
        return ERROR;
    
}
/*******************************gpio********************************************/

/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param GPIOP [description]
 * @return [description]
 */
// ErrorStatus getGpioSlewRate(int16_t GPIOP) {
//     if (BBB_HEADER[GPIOP].PINMUXADD) {
//         volatile PINMUX_REG *GPIO1_PINMUX = MMAP_libfx + (BBB_HEADER[GPIOP].PINMUXADD - PMMap_START_ADDR);
//         return (int16_t) GPIO1_PINMUX->bit.slewctrl;
//     } else {
//         return ERROR;
//     }
// }

/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param GPIOP [description]
 * @return [description]
 */
// ErrorStatus getGpioRxEn(int16_t GPIOP) {
//     if (BBB_HEADER[GPIOP].PINMUXADD) {
//         volatile PINMUX_REG *GPIO1_PINMUX = MMAP_libfx + (BBB_HEADER[GPIOP].PINMUXADD - PMMap_START_ADDR);
//         return (int16_t) GPIO1_PINMUX->bit.rxactive;
//     } else {
//         return ERROR;
//     }
// }

/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param GPIOP [description]
 * @return [description]
 */
// ErrorStatus getGpioPullType(int16_t GPIOP) {
//     if (BBB_HEADER[GPIOP].PINMUXADD) {
//         volatile PINMUX_REG *GPIO1_PINMUX = MMAP_libfx + (BBB_HEADER[GPIOP].PINMUXADD - PMMap_START_ADDR);
//         return (int16_t) GPIO1_PINMUX->bit.putypesel;
//     } else {
//         return ERROR;
//     }
// }

/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param GPIOP [description]
 * @return [description]
 */
// ErrorStatus getGpioPullEn(int16_t GPIOP) {
//     if (BBB_HEADER[GPIOP].PINMUXADD) {
//         volatile PINMUX_REG *GPIO1_PINMUX = MMAP_libfx + (BBB_HEADER[GPIOP].PINMUXADD - PMMap_START_ADDR);
//         return (int16_t) GPIO1_PINMUX->bit.puden;
//     } else {
//         return ERROR;
//     }

// }

/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param GPIOP [description]
 * @return [description]
 */
// ErrorStatus getGpioMuxMode(int16_t GPIOP) {
//     if (BBB_HEADER[GPIOP].PINMUXADD) {
//         volatile PINMUX_REG *GPIO1_PINMUX = MMAP_libfx + (BBB_HEADER[GPIOP].PINMUXADD - PMMap_START_ADDR);
//         return (int16_t) GPIO1_PINMUX->bit.mmode;
//     } else {
//         return ERROR;
//     }
// }


