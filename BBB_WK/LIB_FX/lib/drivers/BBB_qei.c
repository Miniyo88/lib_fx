/**
 ******************************************************************************
 * @file
 * @author  FREDY ALEXANDER SUÁREZ CÁRDENAS
 * @version V
 * @date    05-March-2012
 * @brief
 *
 ******************************************************************************
 * @attention
 *
 *  COPYRIGHT 2013 FREDY ALEXANDER SUÁREZ CÁRDENAS
 *
 *
 ******************************************************************************
 */
#include "BBB.h"


#define KEYLEN 32

int8_t ocp_dir[32];
int8_t eqep_dir[128];
int16_t qei_initialized[nQEIDEV] = {0, 0, 0};

/******************************************************************************/

struct qei_exp
{
    int16_t QEI;
    int16_t enable_fd;
    int16_t mode_fd;
    int16_t period_fd;
    int16_t position_fd;
    uint32_t qei_QPOSCNT;
    struct qei_exp *next;
};
struct qei_exp *exported_qei = NULL;

/******************************************************************************/

/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param QEIMOD [description]
 * @return [description]
 */
struct qei_exp *lookup_exported_qei(int16_t QEIMOD)
{
    struct qei_exp *qei = exported_qei;

    while (qei != NULL)
    {
        if (qei->QEI == (QEIMOD + 1))
        {
            return qei;
        }
        qei = qei->next;
    }
    return NULL; /* standard for pointers */
}
/******************************************************************************/

/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param QEIMOD [description]
 * @return [description]
 */
ErrorStatus load_dev_qei(int16_t QEIMOD)
{
    int16_t status = 0;

    if (QEIMOD == 0)
    {
        printf("qei0\r\n");
        if (!qei_initialized[0] && load_device_tree(QEI0_CAPE))
        {

            printf("qei0:ok\r\n");
            qei_initialized[0] = 1;
            status = 1;
        }
    }
    else if (QEIMOD == 1)
    {
        if (!qei_initialized[1] && load_device_tree(QEI1_CAPE))
        {

            qei_initialized[1] = 1;
            status = 1;
        }
    }
    else if (QEIMOD == 2)
    {
        if (!qei_initialized[2] && load_device_tree(QEI2_CAPE))
        {

            qei_initialized[2] = 1;
            status = 1;
        }
    }
    else
    {
        printf("ERROR QEI CAPE DOES NOT EXIST\r\n");
        return ERROR;
    }
    if (status == 1)
    {
        build_path((char *) "/sys/devices", "ocp", (char *) ocp_dir, sizeof (ocp_dir));
        return SUCCESS;
    }
    return ERROR;
}
/******************************************************************************/

/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param QEIMOD [description]
 * @return [description]
 */
ErrorStatus LoadQEI(int16_t QEIMOD)
{
    int8_t *qei_path_p = NULL;
    int8_t qei_test_path[64];
    int8_t enable_path[64];
    int8_t mode_path[64];
    int8_t period_path[64];
    int8_t position_path[64];
    uint32_t mmap_qeipos;
    int16_t enable_fd, mode_fd, period_fd, position_fd;
    struct qei_exp *new_qei, *qei;

    if (QEIMOD == 0)
    {
        qei_path_p = (int8_t *) QEI0_PATH;
        mmap_qeipos = EQEP_0_REGS;
    }
    else if (QEIMOD == 1)
    {

        qei_path_p = (int8_t *) QEI1_PATH;
        mmap_qeipos = EQEP_1_REGS;
    }
    else if (QEIMOD == 2)
    {

        qei_path_p = (int8_t *) QEI2_PATH;
        mmap_qeipos = EQEP_2_REGS;
    }
    else
    {
        printf("ERROR  QEI \r\n");

        return ERROR;
    }
    if (!qei_initialized[QEIMOD])
    {
        load_dev_qei(QEIMOD);
    }

    if (QEIMOD == 0)
    {

    }
    else if (QEIMOD == 1)
    {

    }
    else if (QEIMOD == 2)
    {

    }
    else
    {
        printf("ERROR QEI #\r\n");
        return ERROR;
    }


    snprintf((char *) qei_test_path, sizeof (qei_test_path), "%s/%s", ocp_dir, qei_path_p);

    //create the path for the period and duty
    snprintf((char *) enable_path, sizeof (enable_path), "%s/enabled", qei_test_path);
    snprintf((char *) mode_path, sizeof (mode_path), "%s/mode", qei_test_path);
    snprintf((char *) period_path, sizeof (period_path), "%s/period", qei_test_path);
    snprintf((char *) position_path, sizeof (position_path), "%s/position", qei_test_path);


    printf("qei%s\r\n", qei_test_path);

    printf("enable_path:%s\r\n", enable_path);

    if ((enable_fd = open((char *) enable_path, O_RDWR)) < 0)
    {
        //error, close already opened period_fd.
        printf("E enable_fd\r\n");
        return ERROR;
    }
    printf("mode_path:%s\r\n", mode_path);
    if ((mode_fd = open((char *) mode_path, O_RDWR)) < 0)
    {
        //error, close already opened period_fd.
        printf("E mode_fd\r\n");
        close(enable_fd);
        return ERROR;
    }

    printf("period_path:%s\r\n", period_path);
    //add period and duty fd to qei list
    if ((period_fd = open((char *) period_path, O_RDWR)) < 0)
    {
        printf("E period_fd\r\n");

        close(mode_fd);
        close(enable_fd);
        return ERROR;
    }
    printf("position_path:%s\r\n", position_path);
    //add period and duty fd to qei list
    if ((position_fd = open((char *) position_path, O_RDWR)) < 0)
    {
        printf("E period_fd\r\n");

        close(period_fd);
        close(mode_fd);
        close(enable_fd);
        return ERROR;
    }

    printf("malloc\r\n");
    // add to list
    new_qei = malloc(sizeof (struct qei_exp));
    if (new_qei == 0)
    {
        printf("E malloc\r\n");
        return ERROR; // out of memory
    }
    new_qei->QEI = (QEIMOD + 1);
    new_qei->enable_fd = enable_fd;
    new_qei->mode_fd = mode_fd;
    new_qei->period_fd = period_fd;
    new_qei->position_fd = position_fd;
    new_qei->qei_QPOSCNT = mmap_qeipos;
    new_qei->next = NULL;


    if (exported_qei == NULL)
    {
        // create new list
        exported_qei = new_qei;
    }
    else
    {
        // add to end of existing list
        qei = exported_qei;
        while (qei->next != NULL)
            qei = qei->next;
        qei->next = new_qei;
    }
    setDisableQEI(QEIMOD);
    return SUCCESS;
}
/******************************************************************************/

/**
 * @brief [brief description]
 * @details [long description]
 */
void closeQEI(void)
{

}
/******************************API*********************************************/

/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param QEIMOD [description]
 * @param freq [description]
 *
 * @return [description]
 */
ErrorStatus setFrecuencyQEI(int16_t QEIMOD, float freq)
{
    int16_t len;
    int8_t buffer[32]; /* allow room for trailing NUL byte */
    uint32_t period_ns;
    struct qei_exp *qei;
    if (freq <= 0.0)
    {
        printf("freq <= 0.0\r\n");
        return ERROR;
    }
    printf("freq:%g\r\n", freq);
    qei = lookup_exported_qei(QEIMOD);
    if (qei == NULL)
    {
        printf("qei==null\r\n");
        return ERROR;
    }
    memset(buffer, 0, 32);
    period_ns = (uint32_t) (1e9 / freq);
    //printf("period:%s:%ld\r\n",qei->key, period_ns);
    len = snprintf((char *) buffer, sizeof (buffer), "%d", period_ns);
    if (write(qei->period_fd, buffer, len) <= 0)
        //printf("period:%s\r\n", buffer);
        return ERROR;
    return SUCCESS;
}

/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param QEIMOD [description]
 * @param mode [description]
 *
 * @return [description]
 */
ErrorStatus setModeQEI(int16_t QEIMOD, int16_t mode)
{
    int16_t len;
    int8_t buffer[7]; /* allow room for trailing NUL byte */
    struct qei_exp *qei;
    qei = lookup_exported_qei(QEIMOD);

    if (qei == NULL)
    {
        return ERROR;
    }
    memset(buffer, 0, 7);
    len = snprintf((char *) buffer, sizeof (buffer), "%d", mode);
    if (write(qei->mode_fd, buffer, len) <= 0)
        //printf("mode:%s\r\n", buffer);
        return ERROR;
    return SUCCESS;
}

/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param QEIMOD [description]
 * @param position [description]
 *
 * @return [description]
 */
ErrorStatus setPositionQEI(int16_t QEIMOD, int32_t value)
{
    int16_t len;
    int8_t buffer[32]; /* allow room for trailing NUL byte */
    struct qei_exp *qei;
    qei = lookup_exported_qei(QEIMOD);

    if (qei == NULL)
    {
        return ERROR;
    }
    if (qei->qei_QPOSCNT == 0)
    {
        printf("error qei\r\n");
        return ERROR;
    }
    memset(buffer, 0, 32);
    len = snprintf((char *) buffer, sizeof (buffer), "%ld", (long int) value);
    if (write(qei->position_fd, buffer, len) <= 0)
        return ERROR;
    return SUCCESS;
}

/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param QEIMOD [description]
 * @return [description]
 */
ErrorStatus getPositionQEI(int16_t QEIMOD , int32_t *value)
{

    struct qei_exp *qei;
    qei = lookup_exported_qei(QEIMOD);

    if (qei == NULL)
    {
        return ERROR;
    }
    if (qei->qei_QPOSCNT == 0)
    {
        printf("error qei\r\n");
        return ERROR;
    }

    *value = HWREG(MMAP_libfx + qei->qei_QPOSCNT - PMMap_START_ADDR);
    return SUCCESS;
}

/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param QEIMOD [description]
 * @return [description]
 */
ErrorStatus qei_disable(int16_t QEIMOD)
{
    struct qei_exp *qei, *temp, *prev_qei = NULL;
    // remove from list
    qei = exported_qei;
    while (qei != NULL)
    {
        if (qei->QEI == (QEIMOD + 1))
        {

            close(qei->enable_fd);
            close(qei->mode_fd);
            close(qei->period_fd);
            close(qei->position_fd);

            if (prev_qei == NULL)
            {
                exported_qei = qei->next;
                prev_qei = qei;
            }
            else
            {
                prev_qei->next = qei->next;
            }

            temp = qei;
            qei = qei->next;
            free(temp);
        }
        else
        {
            prev_qei = qei;
            qei = qei->next;
        }
    }
    return SUCCESS;
}

/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param QEIMOD [description]
 * @param enable [description]
 *
 * @return [description]
 */
ErrorStatus setQEI(int16_t QEIMOD, int16_t enable)
{
    int16_t len;
    int8_t buffer[7]; /* allow room for trailing NUL byte */
    struct qei_exp *qei;
    qei = lookup_exported_qei(QEIMOD);

    if (qei == NULL)
    {
        return ERROR;
    }
    len = snprintf((char *) buffer, sizeof (buffer), "%d", enable);
    if (write(qei->enable_fd, buffer, len) <= 0)
        return ERROR;
    return SUCCESS;
}

/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param QEIMOD [description]
 * @return [description]
 */
ErrorStatus setEnableQEI(int16_t QEIMOD)
{
    return setQEI(QEIMOD, 1);
}

/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param QEIMOD [description]
 * @return [description]
 */
ErrorStatus setDisableQEI(int16_t QEIMOD)
{
    return setQEI(QEIMOD, 0);
}
