/**
 ******************************************************************************
 * @file    
 * @author  FREDY ALEXANDER SUÁREZ CÁRDENAS
 * @version V
 * @date    05-March-2012
 * @brief   
 *
 ******************************************************************************
 * @attention
 *
 *  COPYRIGHT 2013 FREDY ALEXANDER SUÁREZ CÁRDENAS
 *
 *
 ******************************************************************************
 */
#ifndef _HW_ECAP_H_
#define _HW_ECAP_H_

#ifdef __cplusplus
extern "C" {
#endif
#define EQEP_QPOSCNT    0x0000
#define EQEP_QPOSINIT   0x0004
#define EQEP_QPOSMAX    0x0008
#define EQEP_QPOSCMP    0x000C
#define EQEP_QPOSILAT   0x0010
#define EQEP_QPOSSLAT   0x0014
#define EQEP_QPOSLAT    0x0018
#define EQEP_QUTMR      0x001C
#define EQEP_QUPRD      0x0020    
#define EQEP_QWDTMR     0x0024
#define EQEP_QWDPRD     0x0026
#define EQEP_QDECCTL    0x0028
#define EQEP_QEPCTL     0x002A
#define EQEP_QCAPCTL    0x002C
#define EQEP_QPOSCTL    0x002E
#define EQEP_QEINT      0x0030
#define EQEP_QFLG       0x0032
#define EQEP_QCLR       0x0034
#define EQEP_QFRC       0x0036
#define EQEP_QEPSTS     0x0038
#define EQEP_QCTMR      0x003A
#define EQEP_QCPRD      0x003C
#define EQEP_QCTMRLAT   0x003E
#define EQEP_QCPRDLAT   0x0040
#define EQEP_REVID     0x005C
#ifdef __cplusplus
}
#endif

#endif
