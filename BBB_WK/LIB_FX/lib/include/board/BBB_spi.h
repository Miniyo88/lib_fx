/*
 * File:   BBB_spi.h
 * Author: Fredy Alexander Suárez Cárdenas
 *
 * Created on 6 de diciembre de 2013, 11:07 AM
 */

#ifndef BBB_SPI_H
#define BBB_SPI_H

#ifdef  __cplusplus
extern "C" {
#endif

#define SPI0 0
#define SPI1 1
struct spi_exp *lookup_exported_spi(uint8_t SPIMOD);
ErrorStatus load_dev_spi(uint8_t SPIMOD);
ErrorStatus LoadSPI(int16_t SPIMOD);
ErrorStatus setDefaulCfgSPI(uint8_t SPIMOD);
ErrorStatus setSpeedSPI(uint8_t SPIMOD, uint32_t SPEED);
ErrorStatus setBitsPerWordSPI(uint8_t SPIMOD, uint32_t BITSPERWORD);
ErrorStatus WriteReadSPI(uint8_t SPIMOD, uint16_t *data, int16_t length);
ErrorStatus spi_disable(int16_t SPIMOD);

void closeSPI(void);
#ifdef  __cplusplus
}
#endif

#endif  /* BBB_SPI_H */

