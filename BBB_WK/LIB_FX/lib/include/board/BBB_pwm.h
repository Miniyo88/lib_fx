/*
 * File:   BBB_pwm.h
 * Author: Fredy Alexander Suárez Cárdenas
 *
 * Created on 27 de noviembre de 2013, 10:45 PM
 */

#ifndef BBB_PWM_H
#define BBB_PWM_H

#ifdef  __cplusplus
extern "C" {
#endif

ErrorStatus LoadPWM(int16_t GpioPin);
ErrorStatus setStartPWM(int16_t GpioPin);
ErrorStatus setStopPWM(int16_t GpioPin);
ErrorStatus pwm_disable(int16_t GpioPin);
ErrorStatus setDutyPWM(int16_t GpioPin, const float duty);
ErrorStatus setFrecuencyPWM(int16_t GpioPin, const float freq);
ErrorStatus setPolarityPWM(int16_t GpioPin, int16_t polarity);
ErrorStatus runPWM(int16_t GpioPin, int16_t run);
void closePWM(void);
#ifdef  __cplusplus
}
#endif

#endif  /* BBB_PWM_H */
