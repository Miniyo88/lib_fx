/**
 ******************************************************************************
 * @file    
 * @author  FREDY ALEXANDER SUÁREZ CÁRDENAS
 * @version V
 * @date    05-March-2012
 * @brief   
 *
 ******************************************************************************
 * @attention
 *
 *  COPYRIGHT 2013 FREDY ALEXANDER SUÁREZ CÁRDENAS
 *
 *
 ******************************************************************************
 */


volatile void *MMAP_libfx; /*puntero al mapa de memoria*/
short LOADD_MMAP_libfx; /*bandera para indicar si el mapa a sido cargado */
int fd_libfx;

volatile void *Load_libfx(void);
void Close_libfx(void);

int build_path( char *partial_path,  char *prefix, char *full_path, size_t full_path_len);
int load_device_tree( char *name);
int unload_device_tree( char *name);


//---------------------------------------------------------------------------
// Common CPU Definitions:
//

//---------------------------------------------------------------------------
// Include All Peripheral Header Files:
//


#if OMAP335X_DEV_PP

#define OMAP3_GPIO0  1
#define OMAP3_GPIO1  1
#define OMAP3_GPIO2  1
#define OMAP3_GPIO3  1
#define OMAP3_EPWM1  1
#define OMAP3_EPWM2  1
#define OMAP3_EPWM3  1
//#define OMAP3_ECAP1  1
//#define OMAP3_ECAP2  1
//#define OMAP3_ECAP3  1
#define OMAP3_EQEP1  1
#define OMAP3_EQEP2  1
#define OMAP3_I2C  1
#endif  // end 




//#endif  // end of definition*/

/************************ COPYRIGHT 2013 FREDY ALEXANDER SUÁREZ CÁRDENAS *****END OF FILE****/
